FROM hashicorp/terraform:1.4.2

# Create app directory
WORKDIR /codebase

# Copy from existing directory 
COPY . .
